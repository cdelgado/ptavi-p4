#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    dicc = {}
    list = []

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2register()
        print(self.client_address)

        for line in self.rfile:
            print("El cliente nos manda ", str(line.decode('utf-8')))
            var = line.decode('utf-8')
            petition = var.split(' ')
            if petition[0] == 'REGISTER':
                addr = petition[3].split(":")
                IP = self.client_address[0]
                PORT = self.client_address[1]
                EXPIRE = petition[5].split("\r\n")
                tiempo = time.time() + int(EXPIRE[0])
                time_final = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(tiempo))
                var2 = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

            self.dicc[addr[0]] = [IP, PORT, time_final, time.timezone]

            print(self.dicc, '\r\n')

            if time_final <= var2:
                del self.dicc[addr[0]]

        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        self.register2json()

    def register2json(self):
        with open("registered.json", "w") as outfile:
            json.dump(self.dicc, outfile)

    def json2register(self):
        try:
            with open("registered.json", "r") as fileout:
                data = json.load(fileout)
                self.dicc = data
        except KeyError:
            pass
  

if __name__ == "__main__":
    PORT = int(sys.argv[1])
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
