#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


if __name__ == "__main__":
    try:
        PORT = int(sys.argv[2])
        SERVER = (sys.argv[1])
        REGISTER = str(sys.argv[3])
    except IndexError:
        sys.exit("Usage: client.py ip puerto "
                 "register sip_address expires_value")

    if REGISTER == 'register':
        LINE = ("REGISTER sip:" + str(sys.argv[4]) +
                " SIP/2.0\r\n" + "Expires: " + (sys.argv[5]) + '\r\n\r\n')

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.connect((SERVER, PORT))
        print("Enviando:", LINE)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
        print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
